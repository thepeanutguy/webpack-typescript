const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');
const path = require('path');

const config = {
    entry: ['babel-polyfill', './src/index.ts'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: './app.bundle.js',
    },
    resolve: {
        modules: [path.resolve(__dirname, 'node_modules')],
        extensions: ['.tsx', '.ts', '.js'],
    },
    module: {
        rules: [{
            test: /\.tsx?$/,
            exclude: /node_modules/,
            loader: 'ts-loader',
        }, {
            test: /\.scss$/,
            loaders: ['style-loader', 'css-loader', 'sass-loader'],
        }],
    },
    plugins: [
        new UglifyJSPlugin({
            sourceMap: true,
        }),
    ],
};

module.exports = config;
