# Webpack Typescript

A simple & to-the-point Typescript webpack setup. Babel polyfill is included out of the box.

## Requirements

- node.js ^8.10.0
- webpack ^4.3.0
- TypeScript ^2.8.1
- libsass ^3.5.2
- yarn ^1.5.1

## Install

Globally required components:

```bash
$ brew install libsass
$ yarn install typescript --global
```

Local modules:

```bash
$ yarn install
```

## Run

To transpile the TypeScript into a single bundle JavaScript file:

```bash
$ yarn run watch
# or
$ yarn run dev
```

To output a minified production bundle:

```bash
$ yarn run prod
```

If you want to output the bundle file to somewhere else, modify `path` from the `webpack.config.js` file:

```javascript
output: {
    path: path.resolve(__dirname, 'dist'),
    filename: './app.bundle.js'
}
```
